# Alteração de Data de Recurso de Glosa #

### Descrição do Projeto ###
Quando os Segurados utilizam a Rede Credenciada, são gerados determinados custos ou despesas que são repassados à Seguros Unimed.
Estes custos são analisados internamente. Caso sejam identificadas cobranças de itens não conformes ou aderentes, então a Seguros Unimed, formaliza a sua contestação ou não liquidação deste valor (denominado de glosa).

Estes casos são analisados pela área Regulação de Sinistro. A área preenche a planilha no Google Sheets com os processos de Recurso de Glosa que o robô posteriormente deve alterar a data de vencimento no TOP Saúde.

### Conceitos ###
* **Glosa**: Refere-se ao não pagamento de valores em desacordo com o contrato e regras estabelecidas.
* **NR**: Processo recurso glosa 
* **Top Saúde**: Sistema utilizado para processamento de conta médica.
* **Planilha "Alteração de data e fechamento "**: Planilha elaborada pelos analistas, onde são listadas as NR ́s com recurso em aberto e que já foram analisadas.

### Pré-requisitos ###

* Python 3.7
* Pipenv
* Chromedriver (latest)
* Vault

### Instalação & Execução ###

* Preencher o Vault as informações necessárias:

  * URL dos Sistemas
  * Credenciais dos sistemas de homologação e produção
  * Credenciais do Google Sheets
  * Senha do E-mail utilizado pelo robô
* Instalar o pipenv se for necessário:
```
  $ pip install pipenv
```
* Instalar dependências do projeto
```
  $ pipenv install
```
* Executar projeto
```
  $ pipenv run robot
```
### Documentação de Negócio ###

* Link para documentação no Confluence:
  
    * [Alteração de data e Fechamento de Recurso de Glosa](https://ti-segurosunimed-soa.atlassian.net/wiki/spaces/RPA/pages/576454812/B3.+Altera+o+de+Data+e+Fechamento+de+Recurso+de+Glosa?preview=/576454812/910000208/Altera%C3%A7%C3%A3o%20de%20data%20e%20Fechamento%20de%20Recurso%20de%20Glosa%20-%20v2.docx)

### Construído com ###

* [RobotFramework](https://robotframework.org/)
* [Gspread-Pandas](https://gspread-pandas.readthedocs.io/en/latest/using.html)
* [Pipenv](https://realpython.com/pipenv-guide/)

### Autores ###
* **Paola Machado** 