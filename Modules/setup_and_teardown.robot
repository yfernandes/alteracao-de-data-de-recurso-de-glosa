*** Settings ***
Documentation   Keywords de setup e teardown

Resource        ${ROOT}/Resources/main.resource

*** Keywords ***

Setup Screenshot
    Create Directory  ${SCREENSHOT_DIRECTORY}
    Set Screenshot Directory  ${SCREENSHOT_DIRECTORY}

Setup Suite Recurso de Glosa
    ${data_hora_atual}=           Get Current Date  time_zone=local  result_format=%Y%m%d_%H%M%S  exclude_millis=True
    Set Suite Variable  ${data_hora_atual}
    Set Suite Variable  ${glosas_analisadas}  ${0}
    Set Suite Variable  ${glosas_erro}  ${0}
    Set Suite Variable  ${glosas_falha}  ${0}
    Set Suite Variable  ${total_glosas}  ${0}
    Set Suite Variable  ${quantidade_dados_incompletos}  ${0}
    Set Suite Variable  ${motivo_fail}  -
    Setup Screenshot
    Setup Gmail API
    #Empty Directory  ${DATA_DIRECTORY}
    ${inicio_execucao}  Get Current Date  result_format=datetime  exclude_millis=False
    Set Global Variable  ${inicio_execucao}
    Criar Tabelas Kpi


Teardown Test Recurso de Glosa
    Run Keyword If  ${use_gsheets}  Fechar Navegador
    Run Keyword If  ${use_gsheets}  Salvar Alteracoes Planilha
    Coletar Dados da Execução e Inserir nos Metadados
    Run Keyword If  ${use_gsheets}  Salvar Relatorio Metricas
