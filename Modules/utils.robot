***Settings***
Resource        ${ROOT}/Resources/main.resource
*** Keywords ***

Contar Glosa Analisadas com Sucesso
    ${glosas_analisadas}  Evaluate  ${glosas_analisadas} + 1
    Set Test Variable  ${glosas_analisadas}
    Log to File and Console  ${data_hora_atual}  info  Número de Alterações de Data de Glosa com Sucesso: ${glosas_analisadas}

Contar Glosa Analisadas Não Concluída
    ${glosas_falha}  Evaluate  ${glosas_falha} + 1
    Set Test Variable  ${glosas_falha}
    Log to File and Console  ${data_hora_atual}  info  Número de Alterações de Data de Glosa Não Concluída: ${glosas_falha}

Contar Glosa Analisadas Erro de Execução
    ${glosas_erro}  Evaluate  ${glosas_erro} + 1
    Set Test Variable  ${glosas_erro}
    Log to File and Console  ${data_hora_atual}  info  Número de Alterações de Data de Glosa com Erro de Execução: ${glosas_erro}

Inserir KPI no DB
    [Arguments]   ${KPI}
    Log to File and Console  ${data_hora_atual}  info  Inserindo KPI no banco de dados
    ${Id}   Set Variable  RecursoGlosa
    ${Execution}  Get Current Date  result_format=datetime  exclude_millis=False
    Criar Kpi   ${Id}   ${Execution}  ${KPI.total_glosas}  ${KPI.glosas_analisadas}  ${KPI.glosas_falha}  ${KPI.glosas_erro}  ${KPI.glosas_incompletas}  ${KPI.tempo_medio_execucao}  ${KPI.tempo_execucao}  

Coletar Dados da Execução e Inserir nos Metadados
    Extração de Dados e Média da Execução
    &{dict_dados_execucao}  Create Dictionary  total_glosas=${total_glosas}  glosas_analisadas=${glosas_analisadas}
    ...  glosas_falha=${glosas_falha}  glosas_erro=${glosas_erro}  glosas_incompletas=${quantidade_dados_incompletos}
    ...  tempo_medio_execucao=${tempo_medio_execucao}  tempo_execucao=${tempo_execucao}
    Set Suite Metadata  dados_execucao  ${dict_dados_execucao}
    Set Global Variable  ${dict_dados_execucao}
    Inserir KPI no DB  ${dict_dados_execucao}

Extração de Dados e Média da Execução

    ${total_glosas}  Evaluate  ${glosas_analisadas} + ${glosas_falha} + ${glosas_erro}
    ${fim_execucao}  Get Current Date  result_format=datetime  exclude_millis=False
    ${tempo_execucao}  Subtract Date From Date  ${fim_execucao}  ${inicio_execucao}  result_format=timer  exclude_millis=True  date1_format=datetime  date2_format=datetime
    
    ${tempo_medio_execucao}  Subtract Date From Date  ${fim_execucao}  ${inicio_execucao}  result_format=number  exclude_millis=True  date1_format=datetime  date2_format=datetime    
    
    ${tempo_medio_execucao}  Run Keyword If  ${total_glosas} > ${0}  Calcular a Média do Tempo de Execução  ${tempo_medio_execucao}  ${total_glosas}
    ...  ELSE IF  ${total_glosas} == ${0}  Set Variable  ${0}    
    ${tempo_medio_execucao}  Convert Time  ${tempo_medio_execucao}  result_format=timer  exclude_millis=True
    Set Test Variable  ${tempo_medio_execucao}
    Set Test Variable  ${tempo_execucao}
    Set Test Variable  ${fim_execucao}
    Set Test Variable  ${total_glosas}

Calcular a Média do Tempo de Execução
    [Arguments]  ${tempo_medio_execucao}  ${total_glosas}
    ${tempo_medio_execucao}  Evaluate  ${tempo_medio_execucao}/${total_glosas}
    [Return]  ${tempo_medio_execucao}