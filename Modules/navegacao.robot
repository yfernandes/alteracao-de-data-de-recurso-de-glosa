*** Settings ***
Documentation   Biblioteca responsável por realizar o fluxo até abrir a funcionalidade principal do robô

Resource        ${ROOT}/Resources/main.resource

*** Keywords ***

Fechar Navegador
    Close All Browsers

Abrir Navegador
    [Documentation]  Abre o navegador maximizado.
    ...              Escolhi este método por ter melhor performance e pela necessidade
    ...              de carregar uma extensão que desabilita os alertas Js.
    [Arguments]  ${URL}    
    ${options}=  Evaluate  sys.modules['selenium.webdriver'].ChromeOptions()  sys
    Call Method  ${options}  add_argument  start-maximized
    Call Method  ${options}  add_argument  disable-web-security
    Call Method  ${options}  add_argument  disable-notifications
    Call Method  ${options}  add_argument  disable-logging
    Call Method  ${options}  add_argument  lang\=pt-BR
    ${options.binary_location}  Browser Path
    ${chromedriver_path}  Chromedriver Path
    Open Browser  ${URL}  Chrome  options=${options}  executable_path=${chromedriver_path}
    Set Selenium Timeout  ${DEFAULT_SELENIUM_TIMEOUT}
    Log to File and Console  ${data_hora_atual}  info  Abriu o navegador
    Log to File and Console  ${data_hora_atual}  info  Acessou o sistema

Autenticar Usuário
    [Documentation]  Realiza o login do usuário no Top Saúde
    [Arguments]  ${USER}  ${PWD}
    Wait Until Element Is Visible  ${login.usuario}  timeout=None  error=None
    Input Text      ${login.usuario}        ${USER}
    Input Password  ${login.senha}          ${PWD}    
    Click Button    ${login.entrar_btn}
    Log to File and Console  ${data_hora_atual}  info  Autenticou o usuário

Validar Autenticação do Usuário
    ${toast_presente}=  Run Keyword And Return Status
    ...  Wait Until Element Is Visible  ${login.toast}  timeout=3 seconds

    ${botao_presente}=  Run Keyword And Return Status
    ...  Wait Until Element Is Visible  ${login.entrar_btn}  timeout=3 seconds
    Run Keyword If  ${toast_presente} | ${botao_presente}  Set Suite Variable  ${motivo_fail}  Não realizou o login no Top Saúde
    Run Keyword If  ${toast_presente} | ${botao_presente}  Fail  msg=Usuário e Senha Inválidos

Acessar Menu de Quatro Níveis
    [Documentation]  Nageva no menu que contém quatro níveis
    ...              concatenando seu nome com um xpath genérico     
    [Arguments]   ${menu_text}   ${submenu_text}   ${menu_item_text}    ${sub_item_menu_text}
    Unselect Frame
    Wait Until Element Is Not Visible  ${app_locators.loading}
    ${menu_locator}=  Format String  ${sidebar.menu_and_submenu}  menu_texto=${menu_text}
    ${submenu_locator}=  Format String  ${sidebar.menu_and_submenu}  menu_texto=${submenu_text}
    ${menu_item_locator}=  Format String  ${sidebar.menu_and_submenu}  menu_texto=${menu_item_text}
    ${sub_item_locator}=  Format String  ${sidebar.item}  menu_texto=${menu_item_text}  item_texto=${sub_item_menu_text}
    Wait Until Element Is Visible  ${menu_locator}   
    ${menu_expandido}=  Run Keyword And Return Status  Element Should Be Visible  ${submenu_locator}        
    Run Keyword Unless  ${menu_expandido}  Wait Until Element Is Enabled  ${menu_locator}
    Run Keyword Unless  ${menu_expandido}  Click Link  ${menu_locator}
    ${submenu_expandido}=  Run Keyword And Return Status  Element Should Be Visible  ${menu_item_locator}
    Run Keyword Unless  ${submenu_expandido}  Wait Until Element Is Visible  ${submenu_locator}
    Run Keyword Unless  ${submenu_expandido}  Wait Until Element Is Enabled  ${submenu_locator}
    Run Keyword Unless  ${submenu_expandido}  Click Link  ${submenu_locator}  
    Wait Until Element Is Visible  ${menu_item_locator}
    Wait Until Element Is Enabled  ${menu_item_locator}    
    Click Link  ${menu_item_locator}
    Wait Until Element Is Visible  ${sub_item_locator}
    Wait Until Element Is Enabled  ${sub_item_locator}  
    Click Link  ${sub_item_locator}
    Wait Until Element Is Not Visible  ${app_locators.loading}

Aguardar o Loading Desaparecer
    ${loading_presente}=  Run Keyword And Return Status
    ...  Wait Until Element Is Visible  ${app_locators.loading_ajax}  timeout=1 seconds
    Run Keyword If  ${loading_presente}
    ...  Wait Until Element Is Not Visible  ${app_locators.loading_ajax}  timeout=240 seconds

On Error
    [Documentation]  Função responsável por registrar um erro nos logs
    ...              e executar uma função, caso necessário.
    [Arguments]  ${status}  ${err_msg}  ${keyword}=None  @{args}
    Run Keyword If  '${status}' == 'FAIL'  Log to File and Console  ${data_hora_atual}  error  ${err_msg}
    Run Keyword If  '${keyword}' != '${None}' and '${status}' == 'FAIL'  ${keyword}  @{args}
    Run Keyword If  '${status}' == 'FAIL'  Contar Glosa Analisadas Erro de Execução
    Run Keyword If  '${status}' == 'FAIL'  Reload Page


