Documentation   Biblioteca responsável por realizar o fluxo até abrir a funcionalidade principal do robô

Resource        ${ROOT}/Resources/main.resource

*** Keywords ***

Selecionar o Prestador
    [Arguments]  ${codigo_prestador}
    Aguardar o Loading Desaparecer
    Wait Until Element Is Visible  ${pedido.prestador}
    Input Text  ${pedido.prestador}  ${codigo_prestador}
    Press Keys  ${pedido.prestador}  ENTER
    Log to File and Console  ${data_hora_atual}  info  Preencheu o prestador: ${codigo_prestador}

Capturar o nome do prestador
    Aguardar o Loading Desaparecer
    ${nome_do_prestador}  Get Value  ${pedido.nome_prestador}
    Run Keyword If  '${nome_do_prestador}' == '${EMPTY}'  Não Encontrou os Dados Para Realizar a Alteração de Glosa  Prestador
    ...  ELSE  Log to File and Console  ${data_hora_atual}  info  Capturou o nome do prestador: ${nome_do_prestador}
    [Return]  ${nome_do_prestador}

Selecionar a Referência da Rev
    [Arguments]  ${referencia}
    Aguardar o Loading Desaparecer
    Wait Until Element Is Visible  ${pedido.referencia}
    ${status}  Run Keyword And Return Status  Select From List By Label  ${pedido.referencia}  ${referencia}
    Run Keyword If  ${status}  Log to File and Console  ${data_hora_atual}  info  Selecionou a Referência: ${referencia}
    ...  ELSE  Não Encontrou os Dados Para Realizar a Alteração de Glosa  Referência: ${referencia}

Selecionar a NR
    [Arguments]  ${codigo_nr}
    Aguardar o Loading Desaparecer
    Wait Until Element Is Visible  ${pedido.nr}
    ${status}  Run Keyword And Return Status  Select From List By Label  ${pedido.nr}  ${codigo_nr}
    Run Keyword If  ${status}  Log to File and Console  ${data_hora_atual}  info  Selecionou a NR: ${codigo_nr}
    ...  ELSE  Não Encontrou os Dados Para Realizar a Alteração de Glosa  NR: ${codigo_nr}

Selecionar a Solicitação
    [Arguments]  ${solicitacao}
    Aguardar o Loading Desaparecer
    Wait Until Element Is Visible  ${pedido.solicitacao}
    ${status}  Run Keyword And Return Status  Select From List By Label  ${pedido.solicitacao}  ${solicitacao}
    Run Keyword If  ${status}  Log to File and Console  ${data_hora_atual}  info  Selecionou a Solicitação: ${solicitacao}
    ...  ELSE  Não Encontrou os Dados Para Realizar a Alteração de Glosa  Solicitação: ${solicitacao}

Preencher o Data Prev Pgto
    [Arguments]  ${data}
    Aguardar o Loading Desaparecer
    Wait Until Element Is Visible  ${pedido.data_prev_pgto}
    ${data}  Remove String  ${data}  /
    Click Element  ${pedido.data_prev_pgto}
    Input Text  ${pedido.data_prev_pgto}  ${data}
    Press Keys  ${pedido.data_prev_pgto}  TAB
    Log to File and Console  ${data_hora_atual}  info  Preencheu o Campo Data Prev. Pgto: ${data}

Preencher o Motivo
    [Arguments]  ${motivo}
    Aguardar o Loading Desaparecer
    Wait Until Element Is Visible  ${pedido.motivo}
    Input Text  ${pedido.motivo}  ${motivo}
    Log to File and Console  ${data_hora_atual}  info  Preencheu o Motivo: ${motivo}

Preencher a Observação
    [Arguments]  ${observacao}
    Aguardar o Loading Desaparecer
    Wait Until Element Is Visible  ${pedido.observacao}
    Input Text  ${pedido.observacao}  ${observacao}
    Log to File and Console  ${data_hora_atual}  info  Preencheu a observação: ${observacao}

Executar a Alteração do Pedido
    Aguardar o Loading Desaparecer
    Wait Until Element Is Visible  ${pedido.executar}
    Double Click Element  ${pedido.executar}
    Log to File and Console  ${data_hora_atual}  info  Executando a Alteração do Pedido

Capturar a Mensagem da Alteração
    Aguardar o Loading Desaparecer
    ${status}  Run Keyword And Return Status
    ...    Wait Until Element Is Visible  ${pedido.mensagem}    timeout=15 seconds
    ${mensagem}  Run Keyword If  ${status}
    ...    Get Element Attribute  ${pedido.mensagem}  innerText
    Log to File and Console  ${data_hora_atual}  info  Mensagem: ${mensagem}
    [Return]  ${mensagem}

Não Encontrou os Dados Para Realizar a Alteração de Glosa
    [Arguments]  ${mensagem}
    Set Test Variable  ${continuar_alteracao}  ${False}
    Log to File and Console  ${data_hora_atual}  info  Não Encontrou ${mensagem}
    Set Test Variable  ${mensagem_erro}  Não Encontrou ${mensagem}