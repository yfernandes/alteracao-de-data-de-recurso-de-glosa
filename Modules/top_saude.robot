*** Settings ***
Documentation   Keywords de setup e teardown

Resource        ${ROOT}/Resources/main.resource

*** Keywords ***
Recurso de Glosa: Alteração de Data
    [Documentation]   Alteração de Data de Recurso de Glosa
    Autenticação no Gsheets
    Run Keyword If  ${use_gsheets}  Carregar Dados de Execução
    Run Keyword Unless  ${use_gsheets}  Set Suite Variable  ${motivo_fail}   Indisponibilidade Google Sheets
    Run Keyword Unless  ${use_gsheets}  Fail  msg=Gsheets Fora do Ar

    Run Keyword If  ${quantidade_alterações} > 0  Alterar no TopSaúde as Datas de Recurso de Glosa
    ...  ELSE  Log to File and Console  ${data_hora_atual}  info   Não existem dados para inserir no TopSaúde
    
    Run Keyword If  ${quantidade_dados_incompletos} > 0  Atualizar DataFrame Com Dados Incompletos
    ...  ELSE  Log to File and Console  ${data_hora_atual}  info   Não existem Recursos de Glosa com Dados Incompletos

Alterar no TopSaúde as Datas de Recurso de Glosa
    Abrir Navegador  ${URL}
    Autenticar Usuário  ${USER}  ${PWD}
    Validar Autenticação do Usuário
    FOR  ${dado_alteracao}  IN  @{recurso_df_glosa}
        ${status_glosa}  ${err_msg_glosa}  Run Keyword And Ignore Error
        ...  Alterar no TopSaúde a Data de Recurso de Glosa  ${dado_alteracao}
        On Error  ${status_glosa}  ${err_msg_glosa}
    END

Alterar no TopSaúde a Data de Recurso de Glosa
    [Arguments]  ${dado_alteracao}
    Set Test Variable  ${continuar_alteracao}  ${True}
    Set Test Variable  ${mensagem_erro}  ${None}
    Extrair informações da Alteração  ${dado_alteracao}
    Acessar Menu de Quatro Níveis  Contas Médicas  Recurso de Glosa  Abertura de Recurso  Alteração Pedido
    Run Keyword If  ${continuar_alteracao}  Selecionar o Prestador  ${codigo_prestador}
    ${nome_prestador}  Run Keyword If  ${continuar_alteracao}  Capturar o nome do prestador
    Run Keyword If  ${continuar_alteracao}  Selecionar a Referência da Rev  ${referencia}
    Run Keyword If  ${continuar_alteracao}  Selecionar a NR  ${nr}
    Run Keyword If  ${continuar_alteracao}  Selecionar a Solicitação  ${solicitacao}
    Run Keyword If  ${continuar_alteracao}  Preencher o Data Prev Pgto  ${data_pagamento}
    Run Keyword If  ${continuar_alteracao}  Preencher o Motivo  ${motivo}
    Run Keyword If  ${continuar_alteracao}  Preencher a Observação  ${observacao}
    Run Keyword If  ${continuar_alteracao}  Executar a Alteração do Pedido
    ${mensagem_alteracao}  Capturar a Mensagem da Alteração
    Run Keyword If  '${mensagem_alteracao}' == 'None'  Set Test Variable  ${rpa}  ${mensagem_erro}
    ...  ELSE  Set Test Variable  ${rpa}  ${mensagem_alteracao}
    Run Keyword If  '${mensagem_alteracao}' == 'None' and '${mensagem_erro}' == 'None'
    ...    Run Keywords
    ...    Set Test Variable  ${rpa}  Não foi possível realizar a alteração
    ...    AND    Set Test Variable  ${continuar_alteracao}  ${False}
    Run Keyword If  ${continuar_alteracao}  Set Test Variable  ${rpa}  OK
    Atualizar DF Recurso Glosa  ${codigo_prestador}  ${referencia}  ${nr}  RPA1  ${rpa}
    Reload Page
    Run Keyword If  ${continuar_alteracao}  Contar Glosa Analisadas com Sucesso
    Run Keyword Unless  ${continuar_alteracao}  Contar Glosa Analisadas Não Concluída

Extrair informações da Alteração
    [Arguments]  ${dado_alteracao}
    ${codigo_prestador}  Get From Dictionary  ${dado_alteracao}  Prestador
    ${referencia}  Get From Dictionary  ${dado_alteracao}  Referência da Rev.
    ${nr}  Get From Dictionary  ${dado_alteracao}  NR
    ${solicitacao}  Get From Dictionary  ${dado_alteracao}  Solicitação
    ${data_pagamento}  Get From Dictionary  ${dado_alteracao}  Data Prev. Pgto
    ${motivo}  Get From Dictionary  ${dado_alteracao}  Motivo - Alteração
    ${observacao}  Get From Dictionary  ${dado_alteracao}  Observação - Alteração
    ${rpa}  Get From Dictionary  ${dado_alteracao}  RPA1
    Set Test Variable  ${codigo_prestador}
    Set Test Variable  ${referencia}
    Set Test Variable  ${nr}
    Set Test Variable  ${solicitacao}
    Set Test Variable  ${data_pagamento}
    Set Test Variable  ${motivo}
    Set Test Variable  ${observacao}
    Set Test Variable  ${rpa}

Atualizar DataFrame Com Dados Incompletos
    FOR  ${recurso_incompleto}  IN  @{recurso_df_glosa_incompleto}
        ${codigo_prestador}  Get From Dictionary  ${recurso_incompleto}  Prestador
        ${referencia}  Get From Dictionary  ${recurso_incompleto}  Referência da Rev.
        ${nr}  Get From Dictionary  ${recurso_incompleto}  NR
        Atualizar DF Recurso Glosa  ${codigo_prestador}  ${referencia}  ${nr}  RPA1   Dados Incompletos
    END

Autenticação no Gsheets
    FOR    ${index}    IN RANGE    3
        Setup Gsheets
        Exit For Loop If  ${use_gsheets}
        Sleep  ${DEFAULT_GSHEETS_TIMEOUT}
    END

Carregar Dados de Execução
    Carregar Dados Recurso Glosa
    ${quantidade_alterações}=  Get Length  ${recurso_df_glosa}
    Log to File and Console  ${data_hora_atual}  info   Quantidade de Alterações:${quantidade_alterações}
    Set Test Variable  ${quantidade_alterações}

    Carregar Dados Recurso Glosa Incompletos
    ${quantidade_dados_incompletos}=  Get Length  ${recurso_df_glosa_incompleto}
    Set Test Variable  ${quantidade_dados_incompletos}
    Log to File and Console  ${data_hora_atual}  info   Quantidade de Recusos de Glosa Incompletos:${quantidade_dados_incompletos}
    

