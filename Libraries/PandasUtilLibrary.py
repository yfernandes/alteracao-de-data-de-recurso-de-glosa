import pandas as pd
import os
from pathlib import Path
from robot.api import logger
from robot.libraries.BuiltIn import BuiltIn

"""
        Essa biblioteca será retirada em uma futura refatoração.
        A intensão da criação dessa biblioteca era utilizar as funções do
    pandas em um arquivo .robot e, atualmente, só está sendo utilizada em
    arquivos .py
"""


def read_csv_from(csv_file_path, **kwargs):
    """ Lê um arquivo .csv que está dentro do projeto.
    Mais informações:
    https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_csv.html
    :param csv_file_path: caminho do arquivo CSV a partir
    da raiz do projeto.
    :type csv_file_path: str
    :param kwargs: Todos os parâmetro aceitos pelo Pandas.
    :type kwargs: dict, optional
    :return: Objeto DataFrame
    :rtype: DataFrame
    """
    logger.debug(f"Lendo o arquivo: {csv_file_path} \n Parmetros: {kwargs}")
    ROOT_DIR = BuiltIn().get_variable_value("${ROOT}")
    csv_absolute_path = os.path.join(ROOT_DIR, csv_file_path)
    return pd.read_csv(csv_absolute_path, **kwargs)


def build_dataframe(header, row_list):
    return pd.DataFrame(row_list, columns=header)


def filter_dataframe(dataframe, lambda_fn, df_fn=None):
    """ Filtra um Data Frame utilizando uma função lambda e, caso
    houver o parâmetro df_fn, executa a função apply do DataFrame
    com o valor.
    Mais informações:
    https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.apply.html
    :param dataframe: Objeto Data Frame do Pandas com dados.
    :type dataframe: DataFrame
    :param lambda_fn: Função lambda para realizar o filtro
    :type lambda_fn: Lambda ou str
    :param df_fn: Qualquer função que possa ser usada como parâmetro da função
    apply do DataFrame
    :type df_fn: str, optional
    :return: Objeto DataFrame ou tipo do retorno da função passada no df_fn
    :rtype: DataFrame, any
    """
    logger.debug(f"Filtrando DataFrame com o lambda: {lambda_fn}")
    if isinstance(lambda_fn, str):
        lambda_fn = eval(lambda_fn)

    filtered_df = dataframe.loc[lambda_fn]

    if df_fn is not None:
        logger.debug(f"Aplicando a função: {df_fn} no DataFrame")
        return filtered_df.apply(df_fn)
    else:
        return filtered_df


def read_csv(path, **kwargs):
    """ Lê um arquivo .csv que está dentro do projeto.
    Mais informações:
    https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_csv.html
    :param csv_file_path: caminho do arquivo CSV a partir
    da raiz do projeto.
    :type csv_file_path: str
    :param kwargs: Todos os parâmetro aceitos pelo Pandas.
    :type kwargs: dict, optional
    :return: Objeto DataFrame
    :rtype: DataFrame
    """
    logger.debug(f"Lendo o arquivo: {path} \n Parmetros: {kwargs}")
    return pd.read_csv(path, **kwargs)
