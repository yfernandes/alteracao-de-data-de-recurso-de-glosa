from robot.api import logger
import pandas as pd
import gspread
import os
from pathlib import Path, PurePath
from oauth2client.service_account import ServiceAccountCredentials
from gspread_pandas import Client, Spread
from robot.libraries.BuiltIn import BuiltIn
from gmail_client import create_gsecret_json


def setup_gsheets():
    gsheets_id = BuiltIn().get_variable_value("${RELATORIOS_GSHEET_ID}")
    creds_folder = BuiltIn().get_variable_value("${SHEETS_CREDS_PPATH}")
    secrets_folder = \
        BuiltIn().get_variable_value("${GOOGLE_SECRET_JSON_PPATH}")
    GOOGLE_SECRET = BuiltIn().get_variable_value("${GOOGLE_SECRET}")
    create_gsecret_json(secrets_folder, GOOGLE_SECRET)
    creds_file = Path(PurePath(creds_folder, "default"))
    if not creds_file.is_file():
        creds_file.parent.mkdir(parents=True, exist_ok=True)
        G_CREDS = BuiltIn().get_variable_value("${GOOGLE_CREDENTIALS}")
        BuiltIn().set_global_variable("${use_gsheets}", True)
        try:
            with open(creds_file, 'w') as default_file:
                default_file.write(G_CREDS)
        except GspreadPandasException as e:
            BuiltIn().set_global_variable("${use_gsheets}", False)
            logger.error(
                f"Erro ao configurar as credenciais do Google Sheets\n {e}")
        except Exception as e:
            BuiltIn().set_global_variable("${use_gsheets}", False)
            logger.error(f"Erro inesperado ao configurar as credenciais do Google Sheets\n {e}")
        BuiltIn().set_global_variable("${gs_client}", Client())
    else:
        try:
            BuiltIn().set_global_variable("${gs_client}", Client())
        except Exception as e:
            os.remove(creds_file)
            BuiltIn().set_global_variable("${use_gsheets}", False)
            logger.error(f"Erro inesperado ao configurar as credenciais do Google Sheets\n {e}")


def get_gspreadsheet_by_id(spreadsheet_id, **kwargs):
    client = BuiltIn().get_variable_value("${g_client}")
    return Spread(spreadsheet_id, client=client, **kwargs)


def upload_dataframe_gsheets(dataframe, gsheets_id, sheet_name, **kwargs):
    try:
        gspreadsheet = get_gspreadsheet_by_id(gsheets_id, sheet=sheet_name,
                                              **kwargs)
        gspreadsheet.df_to_sheet(dataframe, index=False, sheet=sheet_name,
                                 replace=True)
    except GspreadPandasException as e:
        logger.error(
            f"Erro ao atualizar os dados da Planilha no Google Sheets.\n{e}")
    except Exception as e:
        logger.error(f"Erro inesperado ao configurar as credenciais"
                     "do Google Sheets\n {e}")
