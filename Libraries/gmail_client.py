import pickle
import logging
import base64
from pathlib import Path
from apiclient import errors
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

SCOPES = ['https://mail.google.com/']

logging.getLogger().setLevel(logging.INFO)
logging.getLogger('googleapiclient.discovery_cache').setLevel(logging.ERROR)
logger = logging.getLogger('Seguros Unimed')


def create_gsecret_json(gsecret_json_path, gsecret):
    secret_file_path = Path(gsecret_json_path)
    if not secret_file_path.is_file():
        logger.info('json com id e secret da aplicação não encontrado. Criando'
                    ' no local: {gsecret_json_path}.')
        secret_file_path.parent.mkdir(parents=True, exist_ok=True)
        with open(secret_file_path, 'w') as secrets_json:
            secrets_json.write(gsecret)
        logger.info('Arquivo criado.')


def gmail_credentials(gsecret_json_ppath, token_file_ppath,
                      gmail_auth_port=0):
    creds = None
    gsecret_json_path = Path(gsecret_json_ppath)
    token_file_path = Path(token_file_ppath)
    logger.info('Iniciando construção das credenciais')
    if not gsecret_json_path.is_file():
        logger.error(f'Arquivo json com id e secret do client não'
                     f' encontrado no caminho {gsecret_json_path}')
        raise ValueError(f'Arquivo json com id e secret do client não'
                         f' encontrado no caminho {gsecret_json_path}')
    if token_file_path.is_file():
        logger.info('Arquivo com as credenciais encontrado no sistema.')
        logger.info('Iniciando leitura e coleta das credenciais')
        with open(token_file_path, 'rb') as token:
            creds = pickle.load(token)
            logger.info('Credenciais carregadas')
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            logger.info('Credenciais expiradas. '
                        'Iniciando atualização do token')
            creds.refresh(Request())
            logger.info('Token atualizado')
        else:
            logger.info('Não existe um arquivo de credenciais no sistema')
            logger.info('Iniciando autenticação OAuth2')
            flow = InstalledAppFlow.from_client_secrets_file(
                gsecret_json_path, SCOPES)
            creds = flow.run_local_server(port=gmail_auth_port)
            logger.info('Autenticado')
        with open(token_file_path, 'wb') as token:
            logger.info('Salvando arquivo de credenciais no sistema')
            pickle.dump(creds, token)
            logger.info('Arquivo salvo com sucesso')
    return creds


def build_gmail_service(credentials, api_version='v1'):
    logger.info('Construindo objeto de serviço do Gmail')
    return build('gmail', api_version, credentials=credentials)


def query_emails(gservice, query, user_id='me'):
    try:
        response = gservice.users().messages().list(
            userId=user_id, q=query).execute()
        messages = []
        if 'messages' in response:
            messages.extend(response['messages'])
        while 'nextPageToken' in response:
            page_token = response['nextPageToken']
            response = gservice.users().messages().list(
                userId=user_id, q=query, pageToken=page_token).execute()
            messages.extend(response['messages'])
        return messages
    except errors.HttpError as e:
        logger.error(f'Erro ao listar os emails com a query: {query}'
                     f' {e}')
        return []


def get_email(gservice, email_id, user_id='me'):
    logger.info(f'Coletando e-mail com id {email_id}')
    try:
        email = gservice.users().messages().get(
            userId=user_id, id=email_id).execute()
        return email
    except errors.HttpError as e:
        logger.error(f"Erro ao coletar o email com id {email_id}")


def get_file_attachments_info(gservice, email_id, filter_mime_type=None,
                              user_id='me'):
    try:
        email = get_email(gservice, email_id, user_id=user_id)
        logger.info('E-mail coletado')
        logger.info('Retornando dados do(s) anexo(s)')
        if filter_mime_type:
            return [{"filename": part['filename'],
                     "attachment_id": part['body']['attachmentId'],
                     "email_id": email_id}
                    for part in email['payload']['parts']
                    if 'attachmentId' in part['body']
                    and part['mimeType'] == filter_mime_type]

        return [{"filename": part['filename'],
                 "attachment_id": part['body']['attachmentId'],
                 "email_id": email_id}
                for part in email['payload']['parts']
                if 'attachmentId' in part['body']]
    except Exception as e:
        logger.error(f'Ocorreu um erro ao coletar os ids dos anexos do e-mail'
                     f'com id {email_id} \n{e}')


def download_attachment(gservice, attachment_id,
                        email_id, file_path, user_id='me',
                        encode='UTF-8'):
    try:
        attachment = gservice.users().messages().attachments().get(
            userId=user_id, messageId=email_id, id=attachment_id).execute()

        data = attachment.get('data')
        file_data = base64.urlsafe_b64decode(data.encode(encode))
        with open(file_path, 'wb') as attachment_file:
            attachment_file.write(file_data)
    except Exception as e:
        logger.error(f'Ocorreu um erro ao realizar o download do anexo'
                     f' com id {attachment_id}. \n {e}')


def modify_email_labels(gservice, email_id, labels_payload, user_id='me'):
    try:
        updated_email = \
            gservice.users().messages().modify(userId=user_id, id=email_id,
                                               body=labels_payload).execute()
        return updated_email.get('labelIds')
    except errors.HttpError as e:
        logger.error(f'Ocorreu um erro ao atualizar as labels: '
                     f'{labels_payload} do e-mail com id {email_id}\n {e}')
