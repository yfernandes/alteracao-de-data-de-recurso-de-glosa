from peewee import Model, SqliteDatabase, IntegerField, \
                   CharField, DateField, IntegrityError, \
                   DateTimeField

from datetime import datetime

database_name = "KPI.db"
database = SqliteDatabase(database_name)

class BaseModel(Model):
    class Meta:
        database = database

class KPI(BaseModel):
    
    Robo_ID = CharField()
    Data_Execucao = DateTimeField()
    Total_Analisado = IntegerField()
    Qtd_Sucesso = IntegerField()
    Qtd_Nao_Concluido = IntegerField()
    Qtd_Erro_Execucao = IntegerField()
    Qtd_Incompleto = IntegerField()
    Tempo_Medio_Execucao = DateTimeField()
    Tempo_Total_Execucao = DateTimeField(default=datetime.now())


def criar_kpi(roboid, data_exec, total_analisado, qtd_sucesso, qtd_nao_concluido, qtd_erro_execucao, qtd_incompleto, tempo_medio_exec, tempo_exec):

    try:
        with database.atomic():
          exists = (KPI.select()
                        .where((KPI.Data_Execucao == data_exec) &
                               (KPI.Robo_ID == roboid.lower()))
                        .exists())
          if exists:
              return None
          else:
              return KPI.create(Robo_ID=roboid,
                                Data_Execucao=data_exec,
                                Total_Analisado=total_analisado,
                                Qtd_Sucesso=qtd_sucesso,
                                Qtd_Nao_Concluido=qtd_nao_concluido,
                                Qtd_Erro_Execucao=qtd_erro_execucao,
                                Qtd_Incompleto=qtd_incompleto,
                                Tempo_Medio_Execucao=tempo_medio_exec,
                                Tempo_Total_Execucao = tempo_exec)
    except IntegrityError:
        return None

def criar_tabelas_kpi():
    with database:
        database.create_tables([KPI])

def get_kpi(data_exec, roboid):
    try:
        return KPI.get((KPI.Data_Execucao >= data_exec) & 
                        (KPI.Robo_ID == roboid))
    except KPI.DoesNotExist:
      return None
