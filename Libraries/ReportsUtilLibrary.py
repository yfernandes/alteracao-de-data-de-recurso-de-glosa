import os
import pathlib
import shutil
import pprint
import pandas as pd
import numpy as np
from robot.libraries.BuiltIn import BuiltIn
from robot.api import logger
from PandasUtilLibrary import build_dataframe, read_csv, filter_dataframe
from GsheetsLibrary import get_gspreadsheet_by_id, upload_dataframe_gsheets
from pathlib import Path, PurePath
from gspread_pandas import Spread
from gspread.exceptions import GSpreadException
from gspread_pandas.exceptions import GspreadPandasException


def carregar_dados_recurso_glosa():
    __format_table_columns__()
    __extract_data_from_spreadsheet__()


def __format_table_columns__():
    gspreadsheet_id = \
        BuiltIn().get_variable_value("${RELATORIOS_GSHEET_ID}")
    sheetname = \
        BuiltIn().get_variable_value("${RESULTADOS_SHEET_NAME}")
    try:
        spreadsheet = Spread(gspreadsheet_id)
        worksheet = spreadsheet.spread.worksheet(sheetname)
        worksheet.clear_basic_filter()
        worksheet.format(f'B', {'numberFormat': {'type': 'DATE', 'pattern': 'mm/yyyy'}})
        worksheet.format(f'D', {'numberFormat': {'type': 'DATE', 'pattern': 'dd/mm/yyyy'}})
        worksheet.format(f'E', {'numberFormat': {'type': 'DATE', 'pattern': 'dd/mm/yyyy'}})
        df_recurso_glosa = spreadsheet.sheet_to_df(index=0)
        BuiltIn().set_global_variable("${df_recurso_glosa}",
                                      df_recurso_glosa)
    except Exception as e:
        BuiltIn().set_global_variable("${df_recurso_glosa}",
                                      [])
        logger.warn('Não foi possível capturar os dados na Planilha'
                    ' de Alteração De Recurso de Glosa do GSheets')
        #logger.warn(e)


def __extract_data_from_spreadsheet__():
    df_recurso_glosa = \
        BuiltIn().get_variable_value("${df_recurso_glosa}")
    try:
        if df_recurso_glosa is not None:
            df_result = df_recurso_glosa.loc[((df_recurso_glosa['Prestador'] != '') &
                                            (df_recurso_glosa['Referência da Rev.'] != '') &
                                            (df_recurso_glosa['NR'] != '') &
                                            (df_recurso_glosa['Solicitação'] != '') &
                                            (df_recurso_glosa['Data Prev. Pgto'] != '') &
                                            (df_recurso_glosa['Motivo - Alteração'] != '') &
                                            (df_recurso_glosa['Observação - Alteração'] != '') &
                                            (df_recurso_glosa['RPA1'] == ''))]         
            df_recurso_glosa = df_result[['Prestador', 'Referência da Rev.', 'NR',
                                        'Solicitação', 'Data Prev. Pgto',
                                        'Motivo - Alteração', 'Observação - Alteração',
                                        'RPA1']].to_dict('records')
            BuiltIn().set_global_variable("${recurso_df_glosa}",
                                        df_recurso_glosa)
    except Exception as e:
        BuiltIn().set_global_variable("${recurso_df_glosa}",
                                      [])
        logger.warn('Não foi possível capturar os dados na Planilha'
                    ' de Alteração De Recurso de Glosa do GSheets')
        #logger.warn(e)


def get_processo_recurso_glosa(prestador, referencia, nr_id):
    df_recurso_glosa = \
        BuiltIn().get_variable_value("${df_recurso_glosa}")
    return df_recurso_glosa.loc[((df_recurso_glosa['Prestador'] == prestador) &
                                 (df_recurso_glosa['Referência da Rev.'] == referencia) &
                                 (df_recurso_glosa['NR'] == nr_id))]


def atualizar_df_recurso_glosa(prestador, referencia, nr_id, column, value):
    df_solution = BuiltIn().get_variable_value("${df_recurso_glosa}")
    processo = get_processo_recurso_glosa(prestador, referencia, nr_id)
    row_index = processo.first_valid_index()
    df_solution.loc[row_index, column] = value


def salvar_alteracoes_planilha():
    capturar_df_novo_gheets()
    atualizar_planilha_sheets()


def capturar_df_novo_gheets():
    gspreadsheet_id = \
        BuiltIn().get_variable_value("${RELATORIOS_GSHEET_ID}")
    sheetname = \
        BuiltIn().get_variable_value("${RESULTADOS_SHEET_NAME}")
    try:        
        spreadsheet = Spread(gspreadsheet_id)
        worksheet = spreadsheet.spread.worksheet(sheetname)
        worksheet.clear_basic_filter()
        worksheet.format(f'B', {'numberFormat': {'type': 'DATE', 'pattern': 'mm/yyyy'}})
        worksheet.format(f'D', {'numberFormat': {'type': 'DATE', 'pattern': 'dd/mm/yyyy'}})
        worksheet.format(f'E', {'numberFormat': {'type': 'DATE', 'pattern': 'dd/mm/yyyy'}})
        novo_df_recurso_glosa = spreadsheet.sheet_to_df(index=0)
        BuiltIn().set_global_variable("${novo_df_recurso_glosa}",
                                      novo_df_recurso_glosa)
    except Exception as e:
        logger.warn('Não foi possível capturar os dados na Planilha'
                    ' de Alteração De Recurso de Glosa do GSheets')
        #logger.warn(e)



def atualizar_planilha_sheets():
    novo_df_recurso_glosa = \
        BuiltIn().get_variable_value("${novo_df_recurso_glosa}")
    df_recurso_glosa = BuiltIn().get_variable_value("${df_recurso_glosa}")
    gspreadsheet_id = BuiltIn().get_variable_value("${RELATORIOS_GSHEET_ID}")
    sheetname = BuiltIn().get_variable_value("${RESULTADOS_SHEET_NAME}")
    local_filename = BuiltIn().get_variable_value("${ALTERACAO_LOCAL_FILENAME}")
    try:
        dt_atualizado = pd.concat([novo_df_recurso_glosa, df_recurso_glosa]).drop_duplicates(subset=['Prestador', 'Referência da Rev.', 'NR'], keep='last').sort_index()
        upload_dataframe_gsheets(dt_atualizado, gspreadsheet_id, sheetname)
    except Exception as e:
        logger.warn('Não foi possível atualizar os dados na Planilha'
                    ' de Alteração De Recurso de Glosa do GSheets')
        #logger.warn(e)
        BuiltIn().set_global_variable("${results_backup}",
                                      True)
        logger.info('Planilha enviada por e-mail')
        salvar_relatorio(df_recurso_glosa, local_filename)


def salvar_relatorio_metricas():
    timestamp = \
        BuiltIn().get_variable_value("${fim_execucao}")
    total = \
        BuiltIn().get_variable_value("${total_glosas}")
    processos_sucesso = \
        BuiltIn().get_variable_value("${glosas_analisadas}")
    processos_falha = \
        BuiltIn().get_variable_value("${glosas_falha}")
    processos_incompletos = \
        BuiltIn().get_variable_value("${quantidade_dados_incompletos}")
    processos_erro = \
        BuiltIn().get_variable_value("${glosas_erro}")
    tempo_medio = \
        BuiltIn().get_variable_value("${tempo_medio_execucao}")
    tempo_total = \
        BuiltIn().get_variable_value("${tempo_execucao}")
    gspreadsheet_id = \
        BuiltIn().get_variable_value("${RELATORIOS_GSHEET_ID}")
    sheetname = \
        BuiltIn().get_variable_value("${METRICAS_SHEET_NAME}")
    date = timestamp.strftime("%d/%m/%Y %H:%M:%S")
    row = [date, total, processos_sucesso, processos_falha, processos_erro, processos_incompletos, tempo_medio, tempo_total]
    try:
        gsheets_metricas = get_gspreadsheet_by_id(
            gspreadsheet_id, sheet=sheetname)
        gsheets_metricas.sheet.append_row(
            row, value_input_option='USER_ENTERED')
    except Exception as e:
        logger.warn('Não foi possível registrar as métricas na planilha'
                    ' de Alteração De Recurso de Glosa do GSheets')
        #logger.warn(e)


def carregar_dados_recurso_glosa_incompletos():
    df_recurso_glosa = \
        BuiltIn().get_variable_value("${df_recurso_glosa}")
    try:
        if df_recurso_glosa is not None:
            df_result = df_recurso_glosa.loc[(((df_recurso_glosa['Prestador'] == '') |
                                            (df_recurso_glosa['Referência da Rev.'] == '') |
                                            (df_recurso_glosa['NR'] == '') |
                                            (df_recurso_glosa['Solicitação'] == '') |
                                            (df_recurso_glosa['Data Prev. Pgto'] == '') |
                                            (df_recurso_glosa['Motivo - Alteração'] == '') |
                                            (df_recurso_glosa['Observação - Alteração'] == '')) &
                                            (df_recurso_glosa['RPA1'] == ''))]       
            df_recurso_glosa = df_result[['Prestador', 'Referência da Rev.', 'NR',
                                        'Solicitação', 'Data Prev. Pgto',
                                        'Motivo - Alteração', 'Observação - Alteração',
                                        'RPA1']].to_dict('records')
            BuiltIn().set_global_variable("${recurso_df_glosa_incompleto}",
                                        df_recurso_glosa)            
    except Exception as e:
        logger.error(f"Erro inesperado ao filtrar os dados do dataframe\n")
        #logger.error(f"Erro inesperado ao filtrar os dados do dataframe\n{e}")
        BuiltIn().set_global_variable("${recurso_df_glosa_incompleto}",
                                        [])


def salvar_relatorio(dataframe, filename):
    report_directory = \
        BuiltIn().get_variable_value("${DATA_DIRECTORY}")
    report_path = os.path.join(report_directory, filename)
    dataframe.to_csv(report_path, index=False)
