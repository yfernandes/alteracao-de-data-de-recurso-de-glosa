from robot.libraries.BuiltIn import BuiltIn
from robot.api import logger
from gmail_client import create_gsecret_json, gmail_credentials, \
    build_gmail_service


def setup_gmail_api():
    logger.info("Iniciando configuração e autenticação da API do Gmail")
    google_secret = BuiltIn().get_variable_value("${GOOGLE_SECRET}")
    secret_json_ppath = BuiltIn().get_variable_value(
        "${GOOGLE_SECRET_JSON_PPATH}")
    GMAIL_CREDS_PPATH = BuiltIn().get_variable_value("${GMAIL_CREDS_PPATH}")
    create_gsecret_json(secret_json_ppath, google_secret)
    gcreds = gmail_credentials(secret_json_ppath, GMAIL_CREDS_PPATH)
    service = build_gmail_service(gcreds)
    BuiltIn().set_global_variable("${GMAIL_SERVICE}", service)
    logger.info("Aplicação Autenticada e configurada com sucesso")