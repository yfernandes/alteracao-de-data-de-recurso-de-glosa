import os
import logging
import config


def log_to_file_and_console(filename_date, level, message):
    """
    Cria um arquivo .log e grava as informações no arquivo e exibe as mesmas
    informações no console.

    :param filename_date: data e hora que inicia a execução para gravar o
                          arquivo
    :type path: str
    :param level: nível do log
    :type path: str
    :param message: mensagem
    :type path: str
    """
    # Create a custom logger
    logger = logging.getLogger('Seguros Unimed')

    # Create handlers
    c_handler = logging.StreamHandler()
    try:
        if not os.path.exists(f'{config.ROOT}/Logs'):
            os.makedirs(f'{config.ROOT}/Logs')

        f_handler = logging.FileHandler(f'{config.ROOT}/Logs'
                                        f'/{filename_date}_logfile.log')

        # Create formatters and add it to handlers
        c_format = logging.Formatter('[%(levelname)s]  %(message)s')
        f_format = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s',
                                     datefmt='%Y-%m-%d %H:%M:%S')
        c_handler.setFormatter(c_format)
        f_handler.setFormatter(f_format)

        # Add handlers to the logger
        logger.addHandler(c_handler)
        logger.addHandler(f_handler)

        if level.lower() == 'info':
            c_handler.setLevel(logging.INFO)
            f_handler.setLevel(logging.INFO)

            logger.info(message)

        elif level.lower() == 'debug':
            c_handler.setLevel(logging.DEBUG)
            f_handler.setLevel(logging.DEBUG)

            logger.debug(message)

        elif level.lower() == 'error':
            c_handler.setLevel(logging.ERROR)
            f_handler.setLevel(logging.ERROR)

            logger.error(message)

        # Remove the specified handler from this logger.
        logger.removeHandler(c_handler)
        logger.removeHandler(f_handler)

    except FileNotFoundError:
        print("Erro, arquivo ou diretório inexistente")
    except IOError:
        print("Erro, não foi possível abrir o arquivos")
    except Exception as e:
        print(e)
