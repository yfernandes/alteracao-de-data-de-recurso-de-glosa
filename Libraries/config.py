from pathlib import Path, PurePath
import os
import platform
from datetime import datetime
from Libraries import hvclient

AMBIENTE = os.environ.get('AMBIENTE', 'HML')

if AMBIENTE == 'PRD':
    SEND_EMAIL = True
    RELATORIOS_GSHEET_ID = '1C7UezRNE_kvNJE4xcOPFN_1TRP0idl30EipFBRkksq8'
    TO = ["recursosdeglosas@segurosunimed.com.br", "mauricio.vieira@segurosunimed.com.br"]
    CC = ["paola.machado@primecontrol.com.br"]

else:
    SEND_EMAIL = True
    RELATORIOS_GSHEET_ID = '1fshvWhGYSdXkFa3nqCTnCmVgXXtuEx9AMBL-uxwiJEE'
    TO = ["paola.machado@primecontrol.com.br"]
    CC = ["paola.machado@primecontrol.com.br"]


################ CHROME ##############################
CHROME_VERSION = 85
BROWSER_DIRECTORY = os.environ.get('CHROME').format(CHROME_VERSION)
CHROMEDRIVER_DIRECTORY = os.environ.get('CHROMEDRIVER').format(CHROME_VERSION)
    
################ DIRETÓRIOS LOCAIS ##############################
ROOT = Path(os.path.dirname(os.path.abspath(__file__))).parent
now = datetime.now().strftime("%d%m%Y%H%M%S")
SCREENSHOT_DIRECTORY = os.path.join(ROOT, now)
ALTERACAO_LOCAL_FILENAME = f"Alteracao_{AMBIENTE}.csv"
METRICAS_LOCAL_FILENAME = f"Metricas_{AMBIENTE}.csv"
DATA_DIRECTORY = PurePath(ROOT, "Resources", "Data")

############################  CREDENCIAIS ############################
CREDENTIALS = hvclient.get_credentials('alteracao-de-data-de-recurso-de-glosa')
URL = CREDENTIALS.get(f'{AMBIENTE}_URL')
USER = CREDENTIALS.get(f'{AMBIENTE}_USER')
PWD = CREDENTIALS.get(f'{AMBIENTE}_PWD')
GOOGLE_CREDENTIALS = CREDENTIALS.get('GOOGLE_CREDENTIALS')
GOOGLE_SECRET = CREDENTIALS.get('GOOGLE_SECRET')
GMAIL_CREDS_PPATH = PurePath(ROOT, 'token.pickle')

############################ CONFIGURAÇÕES GOOGLE ############################
os.environ['GSPREAD_PANDAS_CONFIG_DIR'] = ROOT.as_posix()
SHEETS_CREDS_PPATH = PurePath(ROOT, 'creds')
GOOGLE_SECRET_JSON_PPATH = PurePath(ROOT, 'google_secret.json')
RESULTADOS_SHEET_NAME = 'Recurso de Glosa'
METRICAS_SHEET_NAME = 'Métricas'

COLUNAS_RECURSO_GLOSA = ['Prestador', 'NR', 'Motivo - Alteração',
                         'Observação - Alteração', 'RPA1',
                         'Referência da Rev.', 'Solicitação',
                         'Data Prev. Pgto']

use_gsheets = True
results_backup = False

DEFAULT_GSHEETS_TIMEOUT = '60 seconds'

############################ Timeout ############################
DEFAULT_SELENIUM_TIMEOUT = '40 seconds'
DEFAULT_DOWNLOAD_TIMEOUT = '20 seconds'

############################  DATABASE ############################
database_name_execution = 'execution.db'

########################### Email Configuration #################
SMTP_ADDRESS = "smtp.primecontrol.com.br:587"
FROM = "paola.machado@primecontrol.com.br"
SMTP_PASSWORD = CREDENTIALS.get('SMTP_PASSWORD')
SUBJECT = "Relatório de Execução RPA - %s - %s - %s"
TITLE = "Alteração de Data do Recurso de Glosa"
EMAIL_HTML_TEMPLATE_PATH = PurePath(ROOT, 'Listeners', 'email-report.html')
HTML_IMAGES_PATH = PurePath(ROOT, 'Listeners', 'images')

########################### Formats cell #################
DEFAULT_DATE_FORMAT = {
    'numberFormat': {
        "type": 'DATE',
        "pattern": 'DD/MM/YYYY'
    }
}