﻿$env = $Env:AMBIENTE
$scriptpath = $MyInvocation.MyCommand.Path
$curdir = Split-Path $scriptpath
Set-Location $curdir

if($env -eq 'HML')
{
    git fetch --prune
    Write-Output "Environment: $env"
    $releaseBranch = git branch -r | Select-String "release"
    $localReleaseBranch = $releaseBranch -replace "origin/", ""
    Write-Output "Using: $localReleaseBranch"
    git fetch origin $localReleaseBranch.Trim()
    git checkout FETCH_HEAD
}
elseif($env -eq 'PRD')
{   
    Write-Output "Environment: $env"
    git fetch --tags
    $tag = git describe --tags $(git rev-list --tags --max-count=1)
    Write-Output "Release $tag"
    git checkout $tag -B latest
}

Write-Output "Instalando dependências do projeto"
 pipenv sync -d
 
Write-Output "Iniciando a execucao do robô"
 pipenv run robot