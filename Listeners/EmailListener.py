import smtplib
import os
import ntpath
import base64
# from ast import literal_eval
from decimal import Decimal, ROUND_HALF_UP
from pathlib import Path
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email import encoders
from datetime import datetime
from robot.api import logger
from robot.libraries.BuiltIn import BuiltIn
from apiclient import errors
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


class EmailListener:

    ROBOT_LISTENER_API_VERSION = 2

    def __init__(self):
        logger.debug("Iniciando Listerner de envio de e-mail")
        self.step_failed = ""
        self.robot_file_path = ""

    def end_keyword(self, name, attrs):
        """
        Armazena o step, caso houver uma falha.
        """
        if attrs['status'] == 'FAIL':
            logger.debug("Coletando dados do step que falhou")
            logger.debug(f"Passo que falhou: {name}")
            # Necessário fazer o split quando o passo está no formato
            # <nome_modulo>.<Nome do Passo>
            # Exemplo: prepara_reembolso.Fazer login no portal
            self.step_failed = name.split('.')[1] if '.' in name else name

    def end_suite(self, name, attrs):
        """
        Cria o email (MIMEMultipart) usando os attributos da suite
        Envia o email utilizando a biblioteca smtplib
        """
        email = self.__create_email(attrs)
        if BuiltIn().get_variable_value("${SEND_EMAIL}"):
            logger.info(f"Preparando e-mail para o robô {name}")
            service = BuiltIn().get_variable_value("${GMAIL_SERVICE}")
            self.__send_email(email, service)

    def __safe_read_file(self, path, mode, **kwargs):
        """Lê um arquivo, tratando as exceções modificando a variável
        SEND_EMAIL do arquivo email_config.py

        :param path: Caminho do arquivo
        :type path: str
        :param mode: Mesmo parâmetro da função open
        :type mode: str
        :param encoding_type: encoding do arquivo
        :type mode: str, optional
        :param on_error_send_email: Quando houver uma falha,
                este parâmetro é utilizado para alterar a variável SEND_EMAIL
                do arquivo email_config. Quando False, não enviará o e-mail.
        :type on_error_send_email: boolean, kwargs optional
        :return: Conteúdo do arquivo ou caso houver uma exceção None.
        :rtype: str ou None
        """
        try:
            logger.debug(f"Iniciando leitura do arquivo {path}")
            if "encoding" in kwargs:
                with open(path, mode, encoding=kwargs.get("encoding")) as f:
                    return f.read()
            else:
                with open(path, mode) as f:
                    return f.read()
        except IOError as e:
            if "on_error_send_email" in kwargs:
                BuiltIn().set_global_variable(
                    "${SEND_EMAIL}", kwargs.get('on_error_send_email'))
                # Todo: Colocar log informando os detalhes do erro
            logger.error(f"Erro na leitura do arquivo {path}")
            logger.error(e)
            return None

    def __build_robot_results(self, suite_attrs):
        """Cria as variáveis que serão utilizadas na linha da tabela
         do resultado.

        :param suite_attrs: Atributos da suíte executada
        :type suite_attrs: dict
        :return: Nome da suíte, Abtributo do css para o HTML,
            mensagem do resultado e
            mais informações do resultado da execução.
        :rtype: dict
        """
        logger.debug("Iniciando construção dos resultados")
        if suite_attrs['status'] == 'FAIL':
            color = "red"
            motivo = BuiltIn().get_variable_value("${motivo_fail}")
            result = f"Execução com Falha: {motivo}"
            BuiltIn().log_to_console(result)
            total_glosas = 0
            glosas_incompletas = 0
            tempo_execucao = '-'
            tempo_medio_execucao = '-'
            nome_robo = BuiltIn().get_variable_value("${TITLE}")
        else:
            dados_execucao = \
                BuiltIn().get_variable_value("${dict_dados_execucao}")
            color = "green"
            result = "Sucesso"
            glosas_incompletas = dados_execucao['glosas_incompletas']
            total_glosas = dados_execucao['total_glosas']
            tempo_execucao = dados_execucao['tempo_execucao']
            tempo_medio_execucao = dados_execucao['tempo_medio_execucao']
            nome_robo = BuiltIn().get_variable_value("${TITLE}")
            results_backup = BuiltIn().get_variable_value('${results_backup}')
            BuiltIn().log_to_console(results_backup)
            if results_backup:
                localfile = BuiltIn().get_variable_value('${DATA_DIRECTORY}')
                filename = BuiltIn().get_variable_value('${ALTERACAO_LOCAL_FILENAME}')
                self.robot_file_path = f'{localfile}/{filename}'
                BuiltIn().log_to_console(self.robot_file_path)
                more_information = \
                    f'Anexo: {ntpath.basename(self.robot_file_path)}'
                logger.debug(f'Resultados obtidos: {nome_robo}, \
                {color}, {result} e {more_information}')

        return {"color": color, "tempo_execucao": tempo_execucao,
                "tempo_medio_execucao": tempo_medio_execucao,
                "result": result, "total_glosas": total_glosas,
                "glosas_incompletas": glosas_incompletas,
                "robot_name": nome_robo}

    def __create_attachment(self, file_path):
        """Cria e alimenta o objeto MIMEBase com o conteúdo do arquivo
         passado no parâmetro

        :param file_path: Caminho para o arquivo que será anexado.
        :type file_path: str
        :return: Objeto com o conteúdo do arquivo ou None, caso houver exceção.
        :rtype: MIMEBase ou None
        """
        logger.debug("Iniciando criação do anexo do email")
        attachment = MIMEBase('application', "octet-stream")
        file_content = self.__safe_read_file(file_path, 'r', )
        if file_content is not None:
            try:
                attachment.set_payload(file_content)
                encoders.encode_base64(attachment)
                attachment.add_header('Content-Disposition', 'attachment',
                                      filename=ntpath.basename(file_path))
                logger.debug("Anexo construido com sucesso")
                return attachment
            except Exception as e:
                logger.error("Falha ao criar o anexo do e-mail")
                logger.error(e)
                return None

    def __send_email(self, email, service, user_id='me'):
        """ Envia o email
        :param email: Objeto alimentado com os dados para envio
        :type email: MIMEMultipart
        :param service: Objeto de serviço do Gmail 
        """
        # If utilizado aqui para caso um erro tenha acontecido no processo de
        # construção do email
        logger.debug("Enviando o e-mail")
        if BuiltIn().get_variable_value("${SEND_EMAIL}"):
            try:
                email = (service.users().messages().send(userId=user_id, body=email)
                        .execute())
            except errors.HttpError as err:
                message_error = ("Erro ao enviar o e-mail"
                                f"Detalhes: {err} ")
                logger.error(message_error)

    def __create_email(self, robot_attrs):
        """Cria e alimenta o objeto MIMEMultipart com os dados do arquivo
         email_config.py, anexa o arquivo gerado na execução, alimenta
         o template HTML e insere na mensagem do email.

        :param file_robot_attrs: Atributos da execução
        :type file_robot_attrs: dict
        :return: Objeto com todos os dados para enviar o e-mail
        :rtype: base64url encoded
        """
        email = MIMEMultipart()
        dados_execucao = \
            self.__build_robot_results(robot_attrs)
        email['Subject'] = BuiltIn().get_variable_value("${SUBJECT}") % \
            (BuiltIn().get_variable_value("${TITLE}"),
             datetime.now().strftime("%d/%m/%Y %H:%M:%S"),
             dados_execucao['result'])
        email['From'] = BuiltIn().get_variable_value("${FROM}")
        email['To'] = ', '.join(BuiltIn().get_variable_value("${TO}"))
        email['Cc'] = ', '.join(BuiltIn().get_variable_value("${CC}"))
        
        if self.robot_file_path != "":
            attachment = self.__create_attachment(self.robot_file_path)
            if attachment is not None:
                email.attach(attachment)
            else:
                more_information = \
                    'Erro ao anexar o arquivo de resultados no e-mail.'

        images_to_attach = self.__images_to_attach()
        for i, image_path in enumerate(images_to_attach, start=1):
            image_content = self.__safe_read_file(image_path, 'rb')
            if image_content is not None:
                image = MIMEImage(image_content)
                image.add_header('Content-ID', f'<image{i}>')
                logger.debug("Imagem criada no email")
                email.attach(image)

        html = self.__safe_read_file(
            BuiltIn().get_variable_value("${EMAIL_HTML_TEMPLATE_PATH}"),
            'r', on_error_send_email=False, encoding='utf-8')
        if BuiltIn().get_variable_value("${SEND_EMAIL}"):
            html_text = html % (dados_execucao['robot_name'],
                                dados_execucao['color'],
                                dados_execucao['result'],
                                dados_execucao['total_glosas'],
                                dados_execucao['glosas_incompletas'],
                                dados_execucao['tempo_medio_execucao'],
                                dados_execucao['tempo_execucao'])
            mime_html = MIMEText(html_text, 'html')
            email.attach(mime_html)

            # Converte em bytes o objeto de e-mail
            email_bytes = base64.urlsafe_b64encode(email.as_string().encode('utf-8'))

            logger.debug("E-mail criado com sucesso")

            # Retorna o objeto de e-mail codificado em base64url
            return {'raw': email_bytes.decode('utf-8')}

    def __images_to_attach(self):
        images_path = BuiltIn().get_variable_value("${HTML_IMAGES_PATH}")
        return Path(images_path).glob('*.png')

    def build_gmail_service(self, credentials, api_version='v1'):
        logger.info('Construindo objeto de serviço do Gmail')
        return build('gmail', api_version, credentials=credentials)